import java.io.IOException;
import java.net.MalformedURLException;

public class Program {
    public static void main(String[] args)
    {

        if (args.length==2)
        {
            String urlString = args[0];
            String path = args[1];
            HtmlRecursiveDownloader downloader = new HtmlRecursiveDownloader();
            try
            {
                downloader.fetchPages(urlString,path);
            }
            catch(IOException e)
            {
                e.printStackTrace();
            }
        }
        else
        {
            System.out.println("you must define URL and filepath in args");
        }


    }


}
