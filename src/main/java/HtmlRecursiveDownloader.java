import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;

public class HtmlRecursiveDownloader
{
    HashSet<String> links = new HashSet<String>();
    LinkedList<String> queue = new LinkedList<String>();
    static Pattern pattern1 = Pattern.compile("<a\\s([^<]*)</a>");
    static Pattern pattern2 = Pattern.compile("href=\"(http|/)[^/][a-zA-Z0-9./:-]*");
    static Pattern rootPattern = Pattern.compile("http.*//[0-9a-zA-Z.-]*");

    public void fetchPages(String initUrl, String savePath) throws IOException
    {
        Matcher matcher = rootPattern.matcher(initUrl);
        String root;
        String html;
        String currentUrl;
        String protocol;
        if (matcher.find())
        {
            String[] parts;
            parts = matcher.group().split("[.]");
            protocol = parts[0].split("://")[0] +"://";
            if (parts.length>2)
            {
                root =parts[parts.length - 2] + "." + parts[parts.length - 1];

            } else
            {
                root = parts[parts.length - 2] + "." + parts[parts.length - 1];
                root =  root.replace(protocol,"");
            }
            System.out.println("root: "+root);
            System.out.println("protocol "+protocol);
        }
        else
        {
            return;
        }
        links.add(initUrl);
        queue.add(initUrl);
        while (queue.size()>0)
        {
            currentUrl = queue.poll();
            try
            {
                html = connectAndGetHtml(currentUrl);
                saveToHtmlFile(html,currentUrl,savePath);
                getLinksFromHtml(html.toLowerCase(),root,currentUrl,protocol);
            } catch(FileNotFoundException e)
            {
                e.printStackTrace();
                System.out.println("Url "+currentUrl+" returned an error");
            }
        }
    }

    private void saveToHtmlFile(String html,String name, String path) throws IOException
    {
        name = name.replace("/","_");
        name = name.replace(":","_");
        int i = 0;
        File f = new File(path+"/"+name+".html");
        while(f.exists() && !f.isDirectory())
        {
           i++;
           f = new File(path+"/"+name+"("+i+")"+".html");
        }
        f.getParentFile().mkdirs();

        try (FileWriter writer = new FileWriter(f))
        {
            writer.write(html);
        }
    }

    private String connectAndGetHtml(String urlString) throws IOException
    {

        String line;
        BufferedReader bufferedReader;
        InputStream inputStream;
        StringBuilder stringBuilder;
        URL url = new URL(urlString);
        HttpURLConnection con = (HttpURLConnection)url.openConnection();
        HttpURLConnection.setFollowRedirects(true);
        con.setRequestProperty("Accept-Encoding","gzip, deflate");
        con.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 5.1; rv:19.0) Gecko/20100101 Firefox/19.0");
        String encoding = con.getContentEncoding();
        inputStream = null;
        try {
            if ((encoding != null) && encoding.equalsIgnoreCase("gzip")) {
                inputStream = new GZIPInputStream(con.getInputStream());
            } else if ((encoding != null) && encoding.equalsIgnoreCase("deflate")) {
                inputStream = new InflaterInputStream(con.getInputStream(), new Inflater(true));
            } else {
                inputStream = con.getInputStream();
            }
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            stringBuilder = new StringBuilder();
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
                stringBuilder.append("\n");
            }
        } finally
        {
            if (inputStream != null) inputStream.close();
        }
        return stringBuilder.toString();
    }

    private void getLinksFromHtml(String html, String root, String currentUrl, String protocol)
    {
        Matcher m1 = pattern1.matcher(html);

        String link;
        while (m1.find())
        {
            Matcher m2 = pattern2.matcher(m1.group(1));
            if (m2.find())
            {

                link = m2.group().substring(6); //trim href="
                if (link.length()>1)
                {
                    System.out.println(link);
                    if (link.contains(root+"/") ||link.endsWith(root))
                    {
                        if (!links.contains(link))
                        {
                            links.add(link);
                            queue.add(link);
                            System.out.println("adding to collection");
                        }
                    }
                    else if (link.startsWith("/"))
                    {

                        link = protocol+root+link;
                        if (!links.contains(link))
                        {
                            links.add(link);
                            queue.add(link);
                            System.out.println("adding modified " + link);
                        }
                    }
                }
            }
        }
    }

}
